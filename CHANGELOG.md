# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://free.plopgrizzly.com/semver/).

## [Unreleased]
### Added
### Removed
### Changed
### Fixed

## [0.1.2] - 2019-05-13
### Fixed
- Broken links.

## [0.1.1] - 2019-05-13
### Fixed
- `cargo-clippy` warnings.

## [0.1.0] - 2019-03-21
### Added
- Audio playback support on Linux.
- Audio recording support on Linux.
